import axios from "axios";
import * as actionTypes from "./actionTypes";
import { GET_PRODUCT_ADDITIONAL_INFO, GET_PRODUCT_DESCRIPTION, GET_PRODUCT_REVIEWS, GET_SHIPPING_INFO } from "../../helpers/apis"

export const getProductAdditionalInfoAction = async id => {
    let productAdditionalInfo = await axios.get(GET_PRODUCT_ADDITIONAL_INFO + id).then(res => res.data).catch(err => console.log("error in getProductAdditionalInfo in stores", err.message));

    return { type: actionTypes.GET_PRODUCT_ADDITIONAL_INFO, productAdditionalInfo: productAdditionalInfo }
}

export const getProductDescriptionAction = async id => {
    let productDescription = await axios.get(GET_PRODUCT_DESCRIPTION + id).then(res => res.data).catch(err => console.log("error in productDescription in store ", err.message));

    return { type: actionTypes.GET_PRODUCT_DESCRIPTION, productDescription: productDescription }
}

export const getProductReviewAction = async id => {
    let productReviews = await axios.get(GET_PRODUCT_REVIEWS + id).then(res => res.data).catch(err => console.log("error in productRiview in store", err.message));

    return { type: actionTypes.GET_PRODUCT_REVIEWS, productReviews: productReviews }
}

export const getProductShippingInfoAction = async id => {
    let shippingInfo = await axios.get(GET_SHIPPING_INFO + id).then(res => res.data).catch(err => console.log("error in Shipping in store", err.message));

    return { type: actionTypes.GET_SHIPPING_INFO, shippingInfo: shippingInfo }
}