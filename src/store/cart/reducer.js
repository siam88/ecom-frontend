import * as actionTypes from "./actionTypes";

const initalState = {
    cartItems: [],
    product: [],
    productIdArray: []
}

export default function (state = initalState, action) {
    switch (action.type) {
        case actionTypes.GET_CART:
            state = {
                ...state,
                cartItems: action.cartItems
            }
            break
        case actionTypes.POST_PRODUCT_TO_CART:
            state = {
                ...state,
                cartItems: [...state.cartItems, action.variant],
                product: [...state.product, action.product],
                productIdArray: [...state.productIdArray, action.productId]
            }

            break

        case actionTypes.POST_ORDER:
            state = {
                ...state,
                cartItems: [],

            }

            break
        default:
            return state;
    }
    return state;
}


