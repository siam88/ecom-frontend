import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import InboxIcon from "@material-ui/icons/Inbox";
import DraftsIcon from "@material-ui/icons/Drafts";

import { Switch, Route } from "react-router-dom";
import departments from "../../departments";
import { Link as RouterLink } from "react-router-dom";
import Link from "@material-ui/core/Link";
import { NavLink } from "react-router-dom";
import "./departments.css";

const switchRoutes = (
  <Switch>
    {departments.map((r, key) => {
      return (
        <Route path={r.layout + r.path} component={r.component} key={key} />
      );
    })}
  </Switch>
);

const styles = theme => ({});

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

function Departments(props) {
  const { classes } = props;
  return (
    <div className="root">
      <div className="heading">
        <h3 className="browse-catagory">CATAGORIES</h3>
      </div>
      <List component="nav">
        {departments.map((route, index) => (
          <NavLink
            to={route.layout + route.path}
            style={{ textDecoration: "none" }}
          >
            <ListItem button key={route.name} className="listItem">
              <ListItemIcon>
                {index % 2 === 0 ? (
                  <InboxIcon className="custom-icon" />
                ) : (
                  <DraftsIcon className="custom-icon" />
                )}
              </ListItemIcon>
              <p className="departmentItem">{route.name}</p>
              <i className="material-icons custom-icon">keyboard_arrow_right</i>
            </ListItem>
          </NavLink>
        ))}
      </List>
    </div>
  );
}

Departments.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Departments);
