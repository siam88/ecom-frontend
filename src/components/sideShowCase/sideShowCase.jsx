import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import styles from "./sideShowcase.module.css";

class SideShowcase extends Component {
  render() {
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={12} md={12}>
            <div className={styles.product1} />
          </Grid>
          <Grid item xs={12} md={12}>
            <div className={styles.product2} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default SideShowcase;
