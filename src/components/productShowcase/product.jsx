import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import styles from "./productShowcase.module.css";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Tooltip from "@material-ui/core/Tooltip";
import { NavLink } from "react-router-dom";
import { PRODUCT_INFO_PATH } from "../../routes/slugs";
import { ROOT_URL } from "../../helpers/apis";

class ProductGroup extends Component {
  state = {
    width: 0,
    height: 0
  };

  updateWindowDimensions = this.updateWindowDimensions.bind(this);

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
    console.log(this.state.width);
  }


  render() {
    const props = this.props;
    const product = this.props.product;
    console.log("product", product)

    const linkTo = {
      pathname: PRODUCT_INFO_PATH,
      product: props.product
    };

    return (
      // <Grid product md={3} sm={3} xs={3}>

      <Card className={styles.productView} >
        <NavLink to={linkTo} className={styles.linkStyle}>
          <CardMedia
            style={{ height: "200px" }}
            image={ROOT_URL + product.thumbnailImages[0]}
          />

          <CardContent className={styles.productContent}>
            <Typography gutterBottom variant="h5" component="h4">
              {product.name}
            </Typography>
          </CardContent>
        </NavLink>

        <div className={styles.tools}>
          <ul>
            <Tooltip
              title={<Typography color="inherit">add to cart</Typography>}
              placement="top-start"
            >
              <li>
                <i class="material-icons">shopping_cart</i>
              </li>
            </Tooltip>
            <Tooltip
              title={<Typography color="inherit">add to favorite</Typography>}
              placement="top-start"
            >
              <li>
                <i class="material-icons">favorite_border</i>
              </li>
            </Tooltip>
            <Tooltip
              title={<Typography color="inherit">view product</Typography>}
              placement="top-start"
            >
              <li>
                <i class="material-icons">zoom_in</i>
              </li>
            </Tooltip>
          </ul>
        </div>
      </Card>
    );
  }
}

export default ProductGroup;
