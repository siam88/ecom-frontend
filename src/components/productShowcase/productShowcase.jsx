import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Hidden from "@material-ui/core/Hidden";
// import Products from "./products";
import Slider from "react-animated-slider";
import "react-animated-slider/build/horizontal.css";
import "normalize.css/normalize.css";
import styles from "./productShowcase.module.css";
import CategoryMenu from "../categoryMenu/categoryMenu";
import ProductSlider from "./productSlider";
// import { url } from "inspector";

class ProductShowcase extends Component {
  state = {
    isHover: false,
    department: this.props.department,
    backupDepartment: this.props.department
  };

  handleHover = () => {
    this.setState({
      isHover: true
    });
  };

  handleMouseOut = () => {
    this.setState({
      isHover: false
    });
  };

  filterProductsByCategory = category => {
    let department = { ...this.state.backupDepartment };
    let products = [...department.products];
    products = products.filter(p => {
      if (p.category.includes(category)) {
        return p;
      }
    });
    department.products = products;
    this.setState({ department });
  };

  render() {
    let department = this.state.department;
    return (
      <div style={{ marginBottom: "100px" }}>
        <Grid container spacing={3}>
          <Grid item md={3}>
            <Hidden smDown implementation="css">
              <div className={styles.featureProductBox}>
                <div
                  className={styles.featureProduct}
                  style={{
                    backgroundImage: `url('http://localhost:3002${
                      department.featureProduct.thumbnailImages
                      }')`
                    // backgroundImage: `url("https://ii1.pepperfry.com/media/catalog/product/d/o/494x544/doloris-stylish-tufted-chair-in-beige-colour-by-dreamzz-furniture-doloris-stylish-tufted-chair-in-be-0ahswj.jpg")`
                  }}
                >
                  {" "}
                </div>
              </div>
            </Hidden>
          </Grid>
          <Grid item md={9} sm={12} xs={12}>
            <Grid container>
              <Grid item xs={12} md={12} sm={12}>
                <div className={styles.category}>
                  <CategoryMenu
                    categories={this.props.department.categories}
                    departmentName={this.props.department.departmentName}
                    filterProduct={this.filterProductsByCategory}
                  />
                </div>
              </Grid>
              <Grid item md={12} xs={12} sm={12}>
                <div style={{ height: "300px", width: "100%" }}>
                  {/* <Slider className={styles.sliderWrapper}>
              
                    {
                      productGroup.map(e=>productGroup)
                    }
                  </Slider> */}
                  <ProductSlider products={department.products} />
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default ProductShowcase;
