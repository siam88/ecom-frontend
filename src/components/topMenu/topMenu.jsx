import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./topMenu.css";
import Grid from "@material-ui/core/Grid";
import Logo from "../pachpaiLogo/pachpaiLogo";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import naves from "./naves";
import MailIcon from "@material-ui/icons/Mail";

const styles = theme => ({
  menu: {
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  mobileMenu: {
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  }
});

class TopMenu extends Component {
  state = {
    mobileOpen: false,
    topMenuClassName: "topMenu"
  };

  componentDidMount() {
    document.addEventListener("scroll", () => {
      if (window.scrollY > 50) {
        this.setState({ topMenuClassName: "topMenuOnScroll" });
      } else {
        this.setState({ topMenuClassName: "topMenu" });
      }
    });
  }

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };

  render() {
    const { classes, theme } = this.props;
    const drawer = (
      <div>
        <List>
          {naves.map((nav, index) => (
            <NavLink to={nav.path} key={index}>
              <ListItem button >
                <ListItemIcon>
                  {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                </ListItemIcon>
                <ListItemText primary={nav.name} />
              </ListItem>
            </NavLink>
          ))}
        </List>
      </div>
    );
    return (
      <div className={this.state.topMenuClassName}>
        <Grid container spacing={24}>
          <Grid item xs={12} md={3}>
            <div className="logo">
              <Logo />
            </div>
          </Grid>
          <Grid item md={7} xs={12}>
            <Grid item xs={12}>
              <Grid item xs={12} className="search-box">
                <Grid container>
                  <Grid item xs={10}>
                    <input type="text" className="search" />
                  </Grid>
                  <Grid item xs={2}>
                    <button className="search-button">
                      <i class="material-icons">search</i>
                    </button>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12} md={12} className={classes.menu}>
                <ul className="menuItems">
                  {naves.map(nav => (
                    <li key={nav.name}>
                      <NavLink to={nav.path}>
                        {nav.name}
                      </NavLink>
                    </li>
                  ))}
                </ul>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} sm={12} className={classes.mobileMenu}>
            <button className="menuButton" onClick={this.handleDrawerToggle}>
              <i class="material-icons">menu</i>
            </button>
            <Hidden smUp implementation="css">
              <Drawer
                container={this.props.container}
                variant="temporary"
                anchor={theme.direction === "rtl" ? "right" : "left"}
                open={this.state.mobileOpen}
                onClose={this.handleDrawerToggle}
                classes={{
                  paper: classes.drawerPaper
                }}
              >
                {drawer}
              </Drawer>
            </Hidden>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(TopMenu);
