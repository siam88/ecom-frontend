import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Icon, { UnfoldLessTwoTone } from "@material-ui/icons";
import marketBox from "../../image/market-box-70x70.png";
import marketWallet from "../../image/market-wallet-70x70.png";
import funFact from "../../image/PinClipart.com_wallet-clip-art_1914876.png";
import styles from "./bottomArea.module.css";

class BottomArea extends Component {
  render() {
    return (
      <div className={styles.bottomArea} >
        <div className={styles.features}>
          <Grid container>
            <Grid item md={3} sm={6} xs={12}>
              <div className={styles.bottomModal}>
                <a href="#">
                  <div className={styles.textPosition}>
                    <img src={marketBox} className={styles.image} />
                    <h3>Fast Shipping</h3>
                    <h5>
                      For fast shipping pay <br />
                      more
                    </h5>
                  </div>
                </a>
              </div>
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <div className={styles.bottomModal}>
                <a href="https://maveteam.com/">
                  <div className={styles.textPosition}>
                    <img src={marketWallet} className={styles.image} />
                    <h3>Save Money</h3>
                    <h5>
                      See The Top Discount <br />
                      Product
                    </h5>
                  </div>
                </a>
              </div>
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <div className={styles.bottomModal}>
                <a href="#">
                  <div className={styles.textPosition}>
                    <img src={marketBox} className={styles.image} />
                    <h3>First Shipping</h3>
                    <h5>
                      For fast shipping pay <br />
                      more
                    </h5>
                  </div>
                </a>
              </div>
            </Grid>
            <Grid item md={3} sm={6} xs={12}>
              <div
                className={styles.bottomModal}
                style={{ borderRight: "none" }}
              >
                <div className={styles.textPosition}>
                  <a href="https://facebook.com/">
                    <div className={styles.textPosition}>
                      <img src={funFact} className={styles.image} />
                      <h3>High rating Saller</h3>
                      <h5>
                        To get fast shipping you <br />
                        have to pay more
                      </h5>
                    </div>
                  </a>
                </div>
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}
export default BottomArea;
