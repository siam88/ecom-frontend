import React, { Component } from 'react';

class ProductRating extends Component {
  rate = [];

  componentWillMount() {
    for (var i = 1; i <= 5; i++) {
      if (i <= this.props.productRating) {
        this.rate.push(true);
      } else {
        this.rate.push(false);
      }
    }
  }
  render() {
    return (
      <div>
        {this.rate.map(e => {
          if (e === true) return <i className='material-icons'>star</i>;
          else return <i className='material-icons'> star_border</i>;
        })}
      </div>
    );
  }
}

export default ProductRating;
