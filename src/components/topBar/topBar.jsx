import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Cart from "../../views/cartView/cart/cart";
import * as path from "../../routes/slugs";
import "./topBar.css";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";

class TopBar extends Component {
  render() {
    return (
      <div className="topBar">
        <div className="topBarMenu">
          <p className="phone">phone-01966362744</p>
          <ul>
            <li>
              <NavLink to="/account" target="blank">
                MY ACCOUNT
              </NavLink>
            </li>
            <li>
              <a href="#">CONTACT</a>
            </li>
            <li>
              <NavLink to={path.CART_INFO_PATH}>
                CART

              </NavLink>

            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default TopBar;
