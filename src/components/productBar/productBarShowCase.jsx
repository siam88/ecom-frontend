import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import 'react-animated-slider/build/horizontal.css';
import 'normalize.css/normalize.css';
import styles from './productBarShowCase.module.css';
import { NavLink } from 'react-router-dom';

class ProductShowcase extends Component {
  state = {
    isHover: false
  };

  handleHover = () => {
    this.setState({
      isHover: true
    });
  };

  handleMouseOut = () => {
    this.setState({
      isHover: false
    });
  };
  render() {
    return (
      <Grid container style={{ marginBottom: '100px' }}>
        <Grid item xs={12}>
          <h3 className={styles.heading}>
            <span>
              {this.props.showcaseName}
              <NavLink to='#' className={styles.showMore}>
                {' '}
                show more
              </NavLink>
            </span>
          </h3>
          <hr className={styles.hr} />
        </Grid>
        <Grid item xs={12}>
          <Grid container spacing={24}>
            {this.props.products.map((item, index) => {
              const linkTo = {
                pathname: '/product/' + index,
                product: item
              };
              return (
                <Grid item md={1} sm={3} xs={4} key={index}>
                  <NavLink to={linkTo}>
                    <img
                      src={item.image}
                      alt='no image'
                      className={styles.productView}
                    />
                  </NavLink>
                </Grid>
              );
            })}
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default ProductShowcase;
