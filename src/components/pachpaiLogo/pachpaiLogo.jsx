import React, { Component } from "react";
import logo from "../../image/pachpai_logo.png";

const boxStyle = {
  height: "70px",
  lineHeight: "70px"
};

const logoStyle = {
  height: "70px",
  width: "70px"
};

const pachpaiStyle = {
  display: "inline",
  marginLeft: "10px",
  position: "absolute",
  color: "rgb(14, 48, 79)"
};

function Logo() {
  return (
    <div style={boxStyle}>
      <img src={logo} style={logoStyle} />
      <h3 style={pachpaiStyle}>PACHPAI</h3>
    </div>
  );
}

export default Logo;
