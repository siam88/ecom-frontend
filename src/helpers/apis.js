export const ROOT_URL = "http://localhost:3002";
export const VERSION = "v1";

export const API_URL = `${ROOT_URL}/api/${VERSION}`;

//products
export const PRODUCT_URL = `${API_URL}/products`;
export const GET_PRODUCT = `${PRODUCT_URL}/id`;
export const GET_PRODUCT_ADDITIONAL_INFO = `${PRODUCT_URL}/additional-info/`;
export const GET_PRODUCT_DESCRIPTION = `${API_URL}/product-desc/`;
export const GET_PRODUCT_REVIEWS = `${API_URL}/review/`;
export const GET_SHIPPING_INFO = `${API_URL}/shipping/`;
export const GET_HOT_DEPARTMENTS_PRODUCTS = `${PRODUCT_URL}/hot-departments`

//cart
export const CART_URL = `${API_URL}/cart`;
export const GET_CART_URL = `${CART_URL}/`;
export const POST_PRODUCT_TO_CART_URL = `${CART_URL}/`;

//order
export const POST_ORDER_URL = `${API_URL}/orders/post`