import React, { Component } from "react";
import Departments from "../../components/departments/departments";
import { Router, Switch, Route } from "react-router-dom";
import routes from "../../departments";

import TopMenu from "../../components/topMenu/topMenu";
import TopShowcase from "../../components/topShowcase/topShowcase";
import SideShowcase from "../../components/sideShowCase/sideShowCase";
import Grid from "@material-ui/core/Grid";
import { withStyles, withTheme } from "@material-ui/core/styles";
import "./header.css";

const styles = theme => ({
  department: {
    float: "left",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  sideShowcase: {
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  topShowcase: {
    [theme.breakpoints.down("sm")]: {
      display: "block"
    }
  }
});

class Header extends Component {
  render() {
    const { classes, theme } = this.props;
    return (
      <div>
        <div className="header">
          <Grid container spacing={3}>
            <Grid item md={3} className={classes.department}>
              <Departments />
            </Grid>
            <Grid item xs={12} md={7} sm={12} className={classes.topShowcase}>
              <TopShowcase style={{ float: "left" }} />
            </Grid>
            <Grid item xs={12} md={2} className={classes.sideShowcase}>
              <SideShowcase style={{ float: "left" }} />
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Header);
