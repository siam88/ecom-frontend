import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import ProductShowcase from "../../components/productShowcase/productShowcase";
import Progress from "./progress";
import "./showcase.css";
import { GET_HOT_DEPARTMENTS_PRODUCTS } from "../../helpers/apis";

import axios from "axios";

const departmentNames = ["Fashion", "Sports", "Electronic"];
class Showcase extends Component {
  state = {
    departments: [],
    productLoadingError: "",
    loading: false
  };
  componentDidMount() {
    this.setState({ loading: true });
    axios
      .get(GET_HOT_DEPARTMENTS_PRODUCTS)
      .then(res => {
        this.setState({ departments: res.data, loading: false }, () =>
          console.log(this.state.departments)
        );
      })
      .catch(error => {
        this.setState({
          productLoadingError: error.message
        });
      });
  }

  render() {
    return (
      <div className="showcase">
        <Grid container spacing={3}>
          <Grid item xs={12} md={12}>
            {this.state.loading ? (
              <Progress />
            ) : (
                this.state.departments.map((department, i) => (
                  <ProductShowcase department={department} key={i} />
                ))
              )}
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default Showcase;
