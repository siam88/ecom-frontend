import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import styles from './summary.module.css';
import Button from '@material-ui/core/Button';
import { Order } from "../order/order";
import { postOrderAction } from "../../../../store/cart/actions";


export function CartSummary(props) {
    // let product = useSelector(state => state.cartReducer.product)
    const dispatch = useDispatch();
    const [orderedProducts, setOrderedProducts] = useState([]);

    // console.log("items inside cart-summary---", props.products)
    const order = async () => {
        props.products && props.products.map((e, i) => {
            let orderObject = {
                productId: props.productIdArray[i],
                productVariantId: e._id,
                quantity: e.cartQuantity

            }
            orderedProducts.push(orderObject)

        })
        console.log("orderedProducts=======", orderedProducts)
        let orderObj = {
            customerId: "5d91bde778785a2e35c77f8a",
            orderedProducts: orderedProducts

        }



        dispatch(await postOrderAction(orderObj));
    }



    return (
        <div className={styles.box} >
            <h1>Summary</h1>
            <hr />

            <div className={styles.infoContainer} >
                <div className={styles.left} >Subtotal</div> <div className={styles.right}>{props.totalCostWithoutDiscount}</div>
            </div>
            <hr />

            <div className={styles.infoContainer} >
                <div className={styles.left}>Shipping</div> <div className={styles.right}>{props.shippingCost}</div>
            </div>
            <hr />

            <div className={styles.infoContainer} >
                <div className={styles.left}>Discount</div> <div className={styles.right}>{props.totalDiscount}</div>
            </div>
            <hr />

            <div className={[styles.infoContainer, , styles.total].join(" ")} >
                <div className={styles.left}>Total</div> <div className={styles.right} >{
                    Number(props.totalCostWithDiscount + props.shippingCost).toFixed(2)}</div>
            </div>
            <hr />
            {props.products.length == 0 ? null : <Button onClick={() => order()}>ORDER</Button>}


        </div>
    );
}
