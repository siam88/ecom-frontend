import React, { useState, useEffect } from 'react'

export default function Image(props) {

    let height = props.height ? props.height : "100%"
    let width = props.width ? props.width : "100%"
    let src = props.src
    let alt = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/No_image_3x4.svg/1280px-No_image_3x4.svg.png"
    if (typeof (src !== undefined)) {
        src = typeof props.src === "string" ? props.src : URL.createObjectURL(props.src);
    } else {
        src = alt
    }

    return (
        <React.Fragment>
            <img src={src} alt="No " height={height} width={width} />
        </React.Fragment>
    )
}