import React, { Component } from "react";
import Header from "../../layouts/header/header";
import Showcase from "../../layouts/showcases/showcase";
import HotProducts from "../../layouts/hotProduct/hotProductBox";
import ProductBar from "../../components/productBar/productBarShowCase";
import BottomArea from "../../components/bottomArea/bottomArea";
import Footer from "../../layouts/footer/footer";
import axios from "axios";
const content = [
  {
    title: "Tortor Dapibus Commodo Aenean Quam",
    description:
      "Nullam id dolor id nibh ultricies vehicula ut id elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec sed odio dui.",
    button: "Discover",
    image: "https://i.imgur.com/DCdBXcq.jpg",
    user: "Erich Behrens",
    userProfile: "https://i.imgur.com/0Clfnu7.png"
  },
  {
    title: "Vulputate Mollis Ultricies Fermentum Parturient",
    description:
      "Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis.",
    button: "Read More",
    image: "https://i.imgur.com/ZXBtVw7.jpg",
    user: "Luan Gjokaj",
    userProfile: "https://i.imgur.com/JSW6mEk.png"
  }
];

class Home extends Component {
  render() {
    return (
      <div>
        <Header />
        <Showcase />
        <HotProducts />
        <ProductBar showcaseName="YOUR WISH LIST" products={content} />
        <ProductBar showcaseName="RECENTLY VIEW" products={content} />
        <Footer />
      </div>
    );
  }
}

export default Home;
