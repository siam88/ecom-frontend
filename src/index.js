import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

import productReducer from "./store/products/reducer";
import departmentReducer from "./store/departments/reducer";
import cartReducer from "./store/cart/reducer";

import NavBar from "./layouts/navBar/navBar";
import "./styles.css";
import "./fontawesome-free-5.8.2-web/css/all.css";
import routes from "./routes/routes";
import Product from "./views/product/product";
import AccountPage from "./views/accountPage/accountPage";
import { TinyButton as ScrollUpButton } from "react-scroll-up-button";



const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const rootReducer = combineReducers({ productReducer, departmentReducer, cartReducer })

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);



const switchRoutes = (
  <Switch>
    {routes.map((r, key) => {
      return (
        <Route path={r.path} component={r.component} key={key} />
      );
    })}
    <Route path="/product-info/" component={Product} />
  </Switch>
);

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <div className="content">
          <NavBar />
          {switchRoutes}
        </div>
        <Route path="/account" component={AccountPage} />
        <Redirect from="" to={"/market-place/home"} />
        {/* <Redirect from="" to={"/home/home"} /> */}
        <ScrollUpButton style={{ background: "white", borderRadius: "50%" }} />
      </div>
    </BrowserRouter>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement
);
